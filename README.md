# Spring-JWT-Demo-1.5
## This is a demo application for Securing REST API using JWT using SpringBoot.

---

### Technologies used
1.  Spring Boot 1.5
2.  Spring Security
3.  JWT
4.  PostgreSQL

