package gaurav.security.springjwtdemo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class OfficeController {

    @GetMapping("/floor1/office1")
    public ResponseEntity<?> enterFloor1Office1() {
        return new ResponseEntity<>("Entered Floor 1 - Office 1", HttpStatus.OK);
    }

    @GetMapping("/floor1/office2")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> enterFloor1Office2() {
        return new ResponseEntity<>("Entered Floor 1 - Office 2", HttpStatus.OK);
    }

    @GetMapping("/floor2")
    public ResponseEntity<?> enterFloor2Office1() {
        return new ResponseEntity<>("Entered Floor 2", HttpStatus.OK);
    }
}
