package gaurav.security.springjwtdemo.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "APP_ROLE")
public class ApplicationRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(unique = true)
    private RoleEnum roleName;

    @ManyToMany(mappedBy = "roles")
    private Set<ApplicationUser> users;

    public ApplicationRole() {
    }

    public ApplicationRole(RoleEnum roleName) {
        this.roleName = roleName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoleEnum getRoleName() {
        return roleName;
    }

    public void setRoleName(RoleEnum roleName) {
        this.roleName = roleName;
    }

    public Set<ApplicationUser> getUsers() {
        return users;
    }

    public void setUsers(Set<ApplicationUser> users) {
        this.users = users;
    }
}
