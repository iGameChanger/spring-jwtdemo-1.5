package gaurav.security.springjwtdemo.model;

public enum RoleEnum {
    ROLE_ADMIN, ROLE_USER
}
