package gaurav.security.springjwtdemo.repository;

import gaurav.security.springjwtdemo.model.ApplicationRole;
import gaurav.security.springjwtdemo.model.RoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ApplicationRoleRepository extends JpaRepository<ApplicationRole, Long> {

    Optional<ApplicationRole> findByRoleName(RoleEnum roleName);
}
