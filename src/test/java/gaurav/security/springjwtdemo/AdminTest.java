package gaurav.security.springjwtdemo;

import gaurav.security.springjwtdemo.model.ApplicationRole;
import gaurav.security.springjwtdemo.model.ApplicationUser;
import gaurav.security.springjwtdemo.model.RoleEnum;
import gaurav.security.springjwtdemo.repository.ApplicationRoleRepository;
import gaurav.security.springjwtdemo.repository.ApplicationUserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AdminTest {

    @Autowired
    private ApplicationRoleRepository roleRepository;

    @Autowired
    private ApplicationUserRepository userRepository;


    @Test
    public void createRoles() {
        try {
            ApplicationRole adminRole = new ApplicationRole(RoleEnum.ROLE_ADMIN);
            ApplicationRole userRole = new ApplicationRole(RoleEnum.ROLE_USER);

            roleRepository.save(adminRole);
            roleRepository.save(userRole);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void insertUser() {
        try {
//            ApplicationUser user = new ApplicationUser("admin", "admin");
//            Set<ApplicationRole> roles = user.getRoles();
//            ApplicationRole roleAdmin = roleRepository.findByRoleName(RoleEnum.ROLE_ADMIN).orElse(new ApplicationRole());
//            roles.add(roleAdmin);
//            user.setRoles(roles);
//            userRepository.save(user);

            ApplicationUser user = new ApplicationUser("gaurav", "gaurav");
            Set<ApplicationRole> roles = user.getRoles();
            ApplicationRole roleUser = roleRepository.findByRoleName(RoleEnum.ROLE_USER).orElse(new ApplicationRole());
            roles.add(roleUser);
            user.setRoles(roles);
            userRepository.save(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
